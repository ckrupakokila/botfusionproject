// src/App.js
import React from 'react';
import './App.css';
import Form from './Components/Form';


function App() {
  return (
    <div className="App">
      <h1>Bot-FusionForm</h1>
      <Form />
      
    </div>
  );
}

export default App;