// src/components/Form.js
import React, { useState } from 'react';
import StepForm from './StepForm';

const Form = () => {
  const [steps, setSteps] = useState([]);
  const [currentStep, setCurrentStep] = useState(0);

  const handleNextStep = () => {
    setCurrentStep(currentStep + 1);
  };

  const handlePreviousStep = () => {
    setCurrentStep(currentStep - 1);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Handle form submission logic here
  };

  return (
    <form onSubmit={handleSubmit}>
      <StepForm
        currentStep={currentStep}
        steps={steps}
        setSteps={setSteps}
      />
      {currentStep > 0 && (
        <button type="button" onClick={handlePreviousStep}>
          Previous
        </button>
      )}
      {currentStep < steps.length - 1 ? (
        <button type="button" onClick={handleNextStep}>
          Next
        </button>
      ) : (
        <button type="submit">Submit</button>
      )}
    </form>
  );
};

export default Form;