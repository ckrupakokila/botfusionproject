// src/components/StepForm.js
import React from 'react';

const StepForm = ({ currentStep, steps, setSteps }) => {
  const handleNameChange = (e) => {
    const updatedStep = { ...steps[currentStep], name: e.target.value };
    setSteps([
      ...steps.slice(0, currentStep),
      updatedStep,
      ...steps.slice(currentStep + 1),
    ]);
  };

  const handleEmailChange = (e) => {
    const updatedStep = { ...steps[currentStep], email: e.target.value };
    setSteps([
      ...steps.slice(0, currentStep),
      updatedStep,
      ...steps.slice(currentStep + 1),
    ]);
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    const updatedStep = { ...steps[currentStep], file };
    setSteps([
      ...steps.slice(0, currentStep),
      updatedStep,
      ...steps.slice(currentStep + 1),
    ]);
  };

  return (
    <div>
      <input
        type="text"
        placeholder="Name"
        value={steps[currentStep]?.name || ''}
        onChange={handleNameChange}
      />
      <input
        type="email"
        placeholder="Email"
        value={steps[currentStep]?.email || ''}
        onChange={handleEmailChange}
      />
      <input
        type="file"
        onChange={handleFileChange}
      />
      
     
    </div>

  );
};

export default StepForm;